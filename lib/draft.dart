import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'category.dart';
import 'categoryView.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class Draft extends StatelessWidget{
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference transactions = FirebaseFirestore.instance.collection('transactions');
  CollectionReference categories = FirebaseFirestore.instance.collection('category');
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  var controller = new MoneyMaskedTextController(leftSymbol: 'R\$ ', decimalSeparator: ',', thousandSeparator: '.');
  final auth = FirebaseAuth.instance;
  var draftValue;
  String category = '';
  bool categoryExists = false;
  bool valueIsValid = false;
  final snackBarCatFailed = SnackBar(content: Text('Essa categoria não existe!'));
  final snackBarEmpty = SnackBar(content: Text('Preencha todos os campos!'));
  final snackBarInvalid = SnackBar(content: Text('Você não possui esse valor!'));
  List userCategories = [];
  num currentBalance = 0;

  Future<void> addTransaction(type, value, category) async {
    final User user = auth.currentUser;
    return transactions
        .add({
      'UserUid': user.uid, // John Doe
      'Value': value, // Stokes and Sons
      'Type': type,
      'Category': category// 42
    })
        .then((value) => print("Transaction Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> updateBalance(value) async {
    final User user = auth.currentUser;
    List usersList = [];
    int iterator = 0;
    String userDocumentId = "";
    currentBalance = 0;
    await users.get().then((querySnapshot){
      querySnapshot.docs.forEach((document){
        usersList.add(document.data());
        if (usersList[iterator]['Uid'] == user.uid){
          userDocumentId = document.id;
          currentBalance = usersList[iterator]['Balance'];
        }
        iterator++;
      });
    });

    currentBalance -= value;

    FirebaseFirestore.instance.collection('users').doc(userDocumentId).update(
        {'Balance': currentBalance});
  }

  Future<String> checkCategory(category) async {
    final User user = auth.currentUser;
    var iterator = 0;
    List categoryList = [];

    await categories.get().then((querySnapshot){
      querySnapshot.docs.forEach((document){
        categoryList.add(document.data());
        if (categoryList[iterator]['Name'] == category && categoryList[iterator]['UserUid'] == user.uid){
          categoryExists = true;
        }
        iterator++;
      });
    });
  }

  Future<String> checkValidValue(value) async {
    final User user = auth.currentUser;
    var iterator = 0;
    List userList = [];

    await users.get().then((querySnapshot){
      querySnapshot.docs.forEach((document){
        userList.add(document.data());
        if (userList[iterator]['Uid'] == user.uid && userList[iterator]['Balance'] > value){
          valueIsValid = true;
        }
        iterator++;
      });
    });
  }

  Future<String> getCategories() async{
    final User user = auth.currentUser;
    var iterator = 0;
    List allCategories = [];
    userCategories = [];
    await categories.get().then((querySnapshot){
      querySnapshot.docs.forEach((document){
        allCategories.add(document.data());
        if (allCategories[iterator]['UserUid'] == user.uid){
          userCategories.add(allCategories[iterator]);
        }
        iterator++;
      });
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Sacar "),
        backgroundColor: Colors.teal[700],
      ),
      backgroundColor: Colors.teal[200],
      body: Center(
          child: Column(
            children: <Widget> [
              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(hintText: "Valor"),
                onChanged: (value) {
                  draftValue = controller.numberValue;
                },
                controller: controller,
              ),
              TextField(
                decoration: InputDecoration(hintText: "Categoria"),
                onChanged: (value) {
                  category = value;
                },
              ),
              FlatButton(
                child: Text("Sacar!"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async => {
                  categoryExists = false,
                  valueIsValid = false,
                  await checkCategory(category),
                  await checkValidValue(draftValue),
                  if (category.isEmpty || category == null || draftValue == 0 || draftValue == null){
                    ScaffoldMessenger.of(context).showSnackBar(snackBarEmpty),
                  } else if (!categoryExists){
                    ScaffoldMessenger.of(context).showSnackBar(snackBarCatFailed),
                  } else if (!valueIsValid){
                    ScaffoldMessenger.of(context).showSnackBar(snackBarInvalid),
                  } else{
                    await addTransaction("Saque", draftValue, category),
                    await updateBalance(draftValue),
                    Navigator.pop(context, currentBalance)
                  }
                },
              ),
              FlatButton(
                child: Text("Nova categoria"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () => {
                  Navigator.push(context, new MaterialPageRoute(
                      builder: (context) => Category()))
                },
              ),
              FlatButton(
                child: Text("Ver categorias"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async => {
                  await getCategories(),
                  Navigator.push(context, new MaterialPageRoute(
                      builder: (context) => CatView(userCategories)))
                },
              ),
            ],
          )
      ),
    );
  }
}