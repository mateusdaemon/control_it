import 'package:flutter/material.dart';

class CatView extends StatelessWidget{
  List categoryList;

  CatView(List categoryList){
    this.categoryList = categoryList;
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Categorias"),
        backgroundColor: Colors.teal[700],
      ),
      backgroundColor: Colors.teal[200],
      body: ListView.builder(
          itemCount: categoryList.length,
          itemBuilder: (context, index){
            return ListTile(
              title: Text("- ${categoryList[index]['Name']}"),
            );
          }
      ),
    );
  }
}