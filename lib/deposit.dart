import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class Deposit extends StatelessWidget{
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference transactions = FirebaseFirestore.instance.collection('transactions');
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  var controller = new MoneyMaskedTextController(leftSymbol: 'R\$ ', decimalSeparator: ',', thousandSeparator: '.');
  final auth = FirebaseAuth.instance;
  var depositValue;
  final snackBarInvalid = SnackBar(content: Text('Valor invalido!'));
  num currentBalance = 0;

  Future<void> addTransaction(type, value, category) async {
    final User user = auth.currentUser;
    return transactions
        .add({
      'UserUid': user.uid, // John Doe
      'Value': value, // Stokes and Sons
      'Type': type,
      'Category': category// 42
    })
        .then((value) => print("Transaction Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> updateBalance(value) async {
    final User user = auth.currentUser;
    List usersList = [];
    int iterator = 0;
    String userDocumentId = "";
    currentBalance = 0;
    await users.get().then((querySnapshot){
      querySnapshot.docs.forEach((document){
        usersList.add(document.data());
        if (usersList[iterator]['Uid'] == user.uid){
          userDocumentId = document.id;
          currentBalance = usersList[iterator]['Balance'];
        }
        iterator++;
      });
    });

    currentBalance += value;

    FirebaseFirestore.instance.collection('users').doc(userDocumentId).update(
        {'Balance': currentBalance});
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Depositar"),
        backgroundColor: Colors.teal[700],
      ),
      backgroundColor: Colors.teal[200],
      body: Center(
          child: Column(
            children: <Widget> [
              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(hintText: "Valor"),
                onChanged: (value) {
                  depositValue = controller.numberValue;
                },
                controller: controller,
              ),
              FlatButton(
                child: Text("Depositar"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async => {
                  if (depositValue == null || depositValue == "0" || depositValue == ""){
                    ScaffoldMessenger.of(context).showSnackBar(snackBarInvalid),
                  }else {
                    await addTransaction("Deposito", depositValue, ""),
                    await updateBalance(depositValue),
                    Navigator.pop(context, currentBalance)
                  }
                },
              ),
            ],
          )
      ),
    );
  }
}