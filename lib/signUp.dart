import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'login.dart';

class SignUp extends StatelessWidget{
  String _email = '', _password = '', _name = '';
  final auth = FirebaseAuth.instance;
  final snackBarLoginFail = SnackBar(content: Text('Cadastro invalido!'));
  final snackBarSignUpIncomplete = SnackBar(content: Text('Preencha todos os campos!'));
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  Widget build(BuildContext context) {
    Future<void> addUser(name, balance) async {
      final User user = auth.currentUser;
      return users
          .add({
        'Uid': user.uid, // John Doe
        'Name': name, // Stokes and Sons
        'Balance': balance // 42
      })
          .then((value) => print("User Added"))
          .catchError((error) => print("Failed to add user: $error"));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastro"),
        backgroundColor: Colors.teal[700],
      ),
      backgroundColor: Colors.teal[200],
      body: Center(
          child: Column(
            children: <Widget> [
              TextField(
                decoration: InputDecoration(hintText: "Email"),
                onChanged: (value) {
                  _email = value.trim();
                },
              ),
              TextField(
                decoration: InputDecoration(hintText: "Senha"),
                onChanged: (value) {
                  _password = value.trim();
                },
                obscureText: true,
              ),
              TextField(
                decoration: InputDecoration(hintText: "Nome"),
                onChanged: (value) {
                  _name = value.trim();
                },
              ),
              FlatButton(
                child: Text("Casdastrar"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async {
                  try{
                    await auth.createUserWithEmailAndPassword(
                        email: _email, password: _password);
                    await addUser(_name, 0);
                    Navigator.push(context, new MaterialPageRoute(builder: (context) => LoginPage()));
                  } on FirebaseAuthException catch (e){
                    if (_email.isEmpty || _password.isEmpty || _name.isEmpty){
                      ScaffoldMessenger.of(context).showSnackBar(snackBarSignUpIncomplete);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(snackBarLoginFail);
                    }
                  }
                },
              ),
              Text('\n->Email deve possuir um formato valido: email@x.com'),
              Text('->A senha deve possuir pelo menos 6 characteres'),
            ],
          )
      ),
    );
  }
}
