import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'home.dart';
import 'signUp.dart';

class LoginPage extends StatelessWidget{
  String _email = '', _password = '';
  final auth = FirebaseAuth.instance;
  final snackBarLoginFail = SnackBar(content: Text('Email ou Senha invalidos!'));
  final snackBarLoginIncomplete = SnackBar(content: Text('Preencha todos os campos!'));
  final TextEditingController email_controller = new TextEditingController();
  final TextEditingController pass_controller = new TextEditingController();
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  String userName;
  var balance;

  Future<String> getUserData() async {
    final User user = auth.currentUser;
    List usersList = [];
    var iterator = 0;

    await users.get().then((querySnapshot){
      querySnapshot.docs.forEach((document){
        usersList.add(document.data());
        if (usersList[iterator]['Uid'] == user.uid){
          userName = usersList[iterator]['Name'];
          balance = usersList[iterator]['Balance'];
        }
        iterator++;
      });
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Login"),
        backgroundColor: Colors.teal[700],
      ),
      backgroundColor: Colors.teal[300],
      body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage('assets/logo.png'),),
              TextField(
                decoration: InputDecoration(hintText: 'Email'),
                onChanged: (value) {
                  _email = value.trim();
                },
                controller: email_controller,
              ),
              TextField(
                decoration: InputDecoration(hintText: 'Senha'),
                onChanged: (value) {
                  _password = value.trim();
                },
                controller: pass_controller,
                obscureText: true,
              ),
              Text("\n"),
              FlatButton(
                child: Text("Log In"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async {
                  try{
                    await auth.signInWithEmailAndPassword(
                      email: _email, password: _password);
                    await getUserData();
                    Navigator.push(context, new MaterialPageRoute(builder: (context) => HomePage(userName, balance)));
                  } on FirebaseAuthException catch (e){
                    if (_email.isEmpty || _password.isEmpty){
                      ScaffoldMessenger.of(context).showSnackBar(snackBarLoginIncomplete);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(snackBarLoginFail);
                    }
                    print(e);
                  }
                },
              ),
              Text("\nNão possui uma conta?"),
              FlatButton(
                child: Text("Cadastre-se"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () => {
                  Navigator.push(context, new MaterialPageRoute(
                      builder: (context) => SignUp()))
                },
              ),
            ],
          )
      ),
    );
  }
}




