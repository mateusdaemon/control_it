import 'package:flutter/material.dart';

class Statement extends StatelessWidget{
  List statementList;

  Statement(List statementList){
    this.statementList = statementList;
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Extrato"),
        backgroundColor: Colors.teal[700],
      ),
      backgroundColor: Colors.teal[200],
      body: ListView.builder(
          itemCount: statementList.length,
          itemBuilder: (context, index){
            return ListTile(
                title: Text("Tipo: ${statementList[index]['Type']}\nValor: RS ${statementList[index]['Value']}\nCategoria: ${statementList[index]['Category']}\n\n"),
            );
          }
        ),
    );
  }
}