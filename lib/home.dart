import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'deposit.dart';
import 'draft.dart';
import 'statement.dart';
import 'balanceView.dart';

class HomePage extends StatelessWidget{
  final auth = FirebaseAuth.instance;
  CollectionReference statement = FirebaseFirestore.instance.collection('transactions');
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  String userName;
  var balance;
  List statementList = [];
  List userStatementList = [];
  List userList = [];

  HomePage(String userName, var balance){
    this.userName = userName;
    this.balance = balance;
  }

  Future<String> getStatement() async{
    userStatementList = [];
    statementList = [];
    final User user = auth.currentUser;
    var iterator = 0;
    await statement.get().then((querySnapshot){
      querySnapshot.docs.forEach((document){
        statementList.add(document.data());
        if (statementList[iterator]['UserUid'] == user.uid){
          userStatementList.add(statementList[iterator]);
        }
        iterator++;
      });
    });
  }

  Future<String> getCurrentBalance() async{
    userList = [];
    final User user = auth.currentUser;
    var iterator = 0;
    await users.get().then((querySnapshot){
      querySnapshot.docs.forEach((document){
        userList.add(document.data());
        if (userList[iterator]['Uid'] == user.uid){
          this.balance = userList[iterator]['Balance'];
        }
        iterator++;
      });
    });
  }

  @override
  Widget build(BuildContext context){
    return WillPopScope(
      child: new Scaffold(
        appBar: AppBar(
          title: Text("Home Page"),
          backgroundColor: Colors.teal[700],
        ),
        backgroundColor: Colors.teal[200],
        body: Center(
          child: Column(
            children: <Widget> [
              Text("\nBem vindo" + " " + this.userName + "!"),
              FlatButton(
                child: Text("Ver saldo"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async => {
                  await getCurrentBalance(),
                  Navigator.push(context, new MaterialPageRoute(
                      builder: (context) => balanceView(this.balance))),
                },
              ),
              FlatButton(
                child: Text("Depositar"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async => {
                  this.balance = await Navigator.push(context, new MaterialPageRoute(
                   builder: (context) => Deposit())),
                },
              ),
              FlatButton(
                child: Text("Sacar"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async => {
                  this.balance = await Navigator.push(context, new MaterialPageRoute(
                builder: (context) => Draft())),
                },
              ),
              FlatButton(
                child: Text("Extrato"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () async {
                  await getStatement();
                  Navigator.push(context, new MaterialPageRoute(builder: (context) => Statement(userStatementList)));
                },
              ),
            ]
          )
        ),
      ),
      onWillPop: () async => showDialog(
        context: context,
        builder: (context) =>
          AlertDialog(title: Text('Voce irá sair! Continuar?'), backgroundColor: Colors.teal[100],actions: <Widget>[
            RaisedButton(
                child: Text('Sim'),
                color: Colors.teal[700],
                onPressed:(){
                  auth.signOut();
                  Navigator.of(context).pop(true);
                },
            ),
            RaisedButton(
                child: Text('Nao'),
                color: Colors.teal[700],
                onPressed: () => Navigator.of(context).pop(false)),
          ])),
    );
  }
}