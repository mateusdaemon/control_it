import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Category extends StatelessWidget{
  String categoryName = '';
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference category = FirebaseFirestore.instance.collection('category');
  final auth = FirebaseAuth.instance;
  final snackBarAddCat = SnackBar(content: Text('Catergoria registrada!'));
  final snackBarCatEmpty = SnackBar(content: Text('Preencha o campo!'));
  List list = ["oi","tudo bem"];

  Future<void> addCategory(name) async {
    final User user = auth.currentUser;
    return category
        .add({
      'UserUid': user.uid, // John Doe
      'Name': categoryName
    })
        .then((value) => print("Category Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Nova Categoria"),
        backgroundColor: Colors.teal[700],
      ),
      backgroundColor: Colors.teal[200],
      body: Center(
          child: Column(
            children: <Widget> [
              TextField(
                decoration: InputDecoration(hintText: 'Nome da categoria'),
                onChanged: (value) {
                  categoryName = value.trim();
                },
              ),
              FlatButton(
                child: Text("Criar"),
                color: Colors.teal[700],
                minWidth: 200,
                onPressed: () => {
                  if (categoryName.isEmpty || categoryName == null){
                    ScaffoldMessenger.of(context).showSnackBar(snackBarCatEmpty),
                  } else {
                    addCategory(categoryName),
                    ScaffoldMessenger.of(context).showSnackBar(snackBarAddCat),
                    Navigator.pop(context)
                  }
                },
              ),
            ],
          )
      ),
    );
  }
}