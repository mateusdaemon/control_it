import 'package:flutter/material.dart';

class balanceView extends StatelessWidget{
  num balance;

  balanceView(num balance){
    this.balance = balance;
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Saldo"),
        backgroundColor: Colors.teal[700],
      ),
      backgroundColor: Colors.teal[200],
      body: Center(
        child: Column(
          children: [
            Text("\n\nSaldo corrente: RS" + "$balance"),
          ],
        ),
      )
    );
  }
}